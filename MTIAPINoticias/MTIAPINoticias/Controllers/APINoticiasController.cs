﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using MTIAPINoticias.Data;
using MTIAPINoticias.Models;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Schema;

namespace MTIAPINoticias.Controllers
{
    public class APINoticiasController : ApiController
    {
        [HttpPost]
        [Route("api/MTI2017/CargaXML")]
        public async Task<Respuesta> CargaXML()
        {
            // Declarando objeto de respuesta
            Respuesta respuesta = new Respuesta();

            try
            {
                // Obteniendo XML desde el request
                XmlDocument xmlDoc = new XmlDocument();
                var bytesXmlDoc = Encoding.GetEncoding("UTF-8").GetBytes(await Request.Content.ReadAsStringAsync());
                var strXmlDoc = Encoding.GetEncoding("UTF-8").GetString(bytesXmlDoc);
                xmlDoc.PreserveWhitespace = true;
                xmlDoc.LoadXml(strXmlDoc);

                // Validando Schema XML
                var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
                var schemaNoticias = Path.Combine(outPutDirectory, "Schema\\Noticias.xsd");

                schemaNoticias = new Uri(schemaNoticias).LocalPath;

                XmlSchemaSet schemas = new XmlSchemaSet();
                schemas.Add("https://mtinews.azurewebsites.net/Noticias", schemaNoticias);

                XDocument xDoc = XDocument.Parse(xmlDoc.OuterXml);

                // Iniciando la validacion del documento XML
                bool errors = false;
                xDoc.Validate(schemas, (o, ex) =>
                {
                    errors = true;
                });

                if (!errors)
                {
                    // Insertando XML en base de datos
                    using (var context = new mtinewsEntities())
                    {
                        context.InsertNoticias(
                            true,
                            xmlDoc.InnerXml);
                    }

                    // Subiendo el archivo XML
                    var container = GetBlobContainerXML();
                    var blob = container.GetBlockBlobReference("Noticias.xml");
                    await blob.UploadTextAsync(xmlDoc.InnerXml);

                    respuesta.Correcto = true;
                    respuesta.Resultado = "Respuesta OK";
                    respuesta.Mensaje = "El XML se ha subido correctamente";
                    respuesta.Detalle = xmlDoc.OuterXml;
                    return respuesta;
                }
                else
                {
                    // Retornando mensaje de error
                    respuesta.Correcto = false;
                    respuesta.Resultado = "Respuesta error";
                    respuesta.Mensaje = "Schema incorrecto";
                    respuesta.Detalle = string.Format("Error: {0}", "Schema incorrecto");
                    return respuesta;
                }
            }
            catch (Exception ex)
            {
                // Retornando mensaje de error
                respuesta.Correcto = false;
                respuesta.Resultado = "Respuesta error";
                respuesta.Mensaje = ex.Message;
                respuesta.Detalle = string.Format("Error: {0}", ex.Message);
                return respuesta;
            }
        }

        private CloudBlobContainer GetBlobContainerXML()
        {
            // Conectando al Blob Storage
            var storageAccount = CloudStorageAccount.Parse("DefaultEndpointsProtocol=https;AccountName=mtinews;AccountKey=MudnbY56YyYYO059wr9gZkP0LQuqphhArkJduH/5pVBPZ4Fu5RavZKmKp7AevjcwK90YJfRpV5wSqAD7ckvrmg==;EndpointSuffix=core.windows.net");
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference("xml");
            container.CreateIfNotExists();
            container.SetPermissions(
                new BlobContainerPermissions
                {
                    PublicAccess = BlobContainerPublicAccessType.Blob
                });
            return container;
        }
    }
}
