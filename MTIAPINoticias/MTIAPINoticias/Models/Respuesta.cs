﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MTIAPINoticias.Models
{
    public class Respuesta
    {
        /// <summary>
        /// Indica si el proceso se ejecuto correctamente o no
        /// </summary>
        public bool Correcto { get; set; }
        /// <summary>
        /// Mensaje dirigido al usuario indicado el estado del proceso
        /// </summary>
        public string Mensaje { get; set; }
        /// <summary>
        /// Detalle del proceso 
        /// </summary>
        public string Detalle { get; set; }
        /// <summary>
        /// Resultado del proceso
        /// </summary>
        public object Resultado { get; set; }
    }
}